from dataclasses import dataclass

from environs import Env


@dataclass
class TGBot:
    token: str
    owner_id: int
    youmoney: str


@dataclass
class DbConfig:
    host: str
    password: str
    user: str
    database: str


@dataclass
class Config:
    tg_bot: TGBot
    db: DbConfig


def load_config(path: str = None):
    env = Env()
    env.read_env(path)

    return Config(
        tg_bot=TGBot(
            token=env.str('TELEGRAM_API_TOKEN'),
            owner_id=env.int('OWNER_ID'),
            youmoney=env.str('YOUMONEY_TOKEN')
        ),
        db=DbConfig(
            host=env.str('POSTGRES_HOST'),
            password=env.str('POSTGRES_PASSWORD'),
            user=env.str('POSTGRES_USER'),
            database=env.str('POSTGRES_DB')
        )
    )

