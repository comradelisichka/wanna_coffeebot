from aiogram.dispatcher.filters import BoundFilter

from config import Config


class OwnerFilter(BoundFilter):
    key = 'is_owner'

    def __init__(self, is_owner=None):
        self.is_owner = is_owner

    async def check(self, obj):
        if self.is_owner is None:
            return
        config: Config = obj.bot.get('config')
        user_id = obj.from_user.id
        return user_id == config.tg_bot.owner_id

