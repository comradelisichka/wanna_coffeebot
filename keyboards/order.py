from aiogram.types import ReplyKeyboardMarkup

order = '☺️ Оформить заказ'
back_message = '🔙 Назад'
ok_message = '☕️ Начинаем варить'


def back_markup():
    markup = ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    markup.add(back_message)

    return markup


def check_markup():
    markup = ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    markup.row(back_message, ok_message)

    return markup
