from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData

items_cd = CallbackData('items', 'name', 'action')


def item_markup(name, count):
    global items_cd

    markup = InlineKeyboardMarkup()
    back_button = InlineKeyboardButton('⬅️', callback_data=items_cd.new(name=name, action='decrease'))
    count_button = InlineKeyboardButton(count, callback_data=items_cd.new(name=name, action='count'))
    next_button = InlineKeyboardButton('➡️', callback_data=items_cd.new(name=name, action='increase'))
    markup.row(back_button, count_button, next_button)

    return markup
