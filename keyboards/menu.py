from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.utils.callback_data import CallbackData

from db_models import Menu

items_cd = CallbackData('items', 'name', 'action')

keyboard_menu = InlineKeyboardMarkup(row_width=1)
menu_gen = [InlineKeyboardButton(text=i, callback_data=items_cd.new(name=i.split(',')[0], action='add'))
            for i in Menu().get_items()]

keyboard_menu.add(*menu_gen)
