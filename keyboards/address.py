from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from db_models import Address
from aiogram.utils.callback_data import CallbackData

address_cd = CallbackData('address', 'place')
keyboard_adrs = InlineKeyboardMarkup(row_width=2)
adrs_gen = [InlineKeyboardButton(text=i, callback_data=address_cd.new(place=i))
            for i in Address().get_list_of_adrs()]

keyboard_adrs.add(*adrs_gen)
