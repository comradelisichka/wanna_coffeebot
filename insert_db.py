import psycopg2

from config import load_config

try:
    connection = psycopg2.connect(
        dbname=load_config('.env').db.database,
        user=load_config('.env').db.user,
        password=load_config('.env').db.password
    )

except:
    print('unable to connect to the db')

cursor = connection.cursor()


def sql_insert():
    global connection, cursor
    data_adrs = [
        ('ул. Садовая, 15', '34231'),
        ('пр. Добролюбова, 4', '34232'),
        ('пер. Зинцова, 54','34233'),
        ('ул. Плющеева, 2', '34234')
        ]
    data_items = [
        ('Латте S', 250, 100),
        ('Латте L', 350, 180),
        ('Капучино S', 250, 100),
        ('Капучино M', 350, 180),
        ('флэт-уайт', 250, 150)
    ]
    #cursor.executemany('INSERT into addresses(name, admin_id) VALUES (%s, %s)', data_adrs)
    # cursor.executemany('INSERT into menu(name, size, price) VALUES (%s, %s, %s)', data_items)
    connection.commit()
    print('tables are completed')
