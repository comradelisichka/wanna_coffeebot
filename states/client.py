from aiogram.dispatcher.filters.state import StatesGroup, State


class FSMClient(StatesGroup):
    check_cart = State()
    confirm = State()
