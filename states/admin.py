from aiogram.dispatcher.filters.state import StatesGroup, State


class FSMOwner(StatesGroup):
    menu_item = State()
    volume = State()
    price = State()
