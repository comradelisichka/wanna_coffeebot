from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text

from states.admin import FSMOwner

import db


async def start_loading(message: types.Message):
    await FSMOwner.first()
    await message.reply('название позиции')


async def cancel_handler(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer('загрузка отменена')


async def load_menu_item(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['name'] = message.text

    await FSMOwner.next()
    await message.reply('объём')


async def load_volume(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['size'] = int(message.text)

    await FSMOwner.next()
    await message.reply('цена')


async def load_price(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['price'] = int(message.text)

        db.insert('menu', data)
        await state.finish()
        await message.answer('готово')


def register_handlers_admin(dp: Dispatcher):
    dp.register_message_handler(start_loading, is_owner=True, commands='загрузить', state=None)

    dp.register_message_handler(cancel_handler, commands='cancel', state="*")
    dp.register_message_handler(cancel_handler, Text(equals='Галя, отмена!', ignore_case=True), state="*")

    dp.register_message_handler(load_menu_item, content_types=['text'], state=FSMOwner.menu_item)

    dp.register_message_handler(load_volume, state=FSMOwner.volume)

    dp.register_message_handler(load_price, state=FSMOwner.price)
