from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.types import ChatActions, ReplyKeyboardMarkup, ContentType

import db
from keyboards import keyboard_adrs, address_cd, keyboard_menu, item_markup, order, ok_message, items_cd, \
    cart, cart_markup, check_markup
from keyboards.order import back_message
from states.client import FSMClient

from db_models import Cart, Menu, Address
from utils import time_checking


async def send_welcome(message: types.Message):
    username = message.from_user.first_name
    await message.answer(
        f'Привет, {username}!\n\n'
        'Мы готовим кофе для жителей Санкт-Петербурга в четырёх кофейнях.\n'
        'Выбери адрес, где сможешь забрать свой кофе:',
        reply_markup=keyboard_adrs
    )


async def adrs_call(callback: types.CallbackQuery, callback_data: dict, state: FSMContext):
    if time_checking() == 'closed':
        await callback.answer('упс, кофейня зкрыта...', show_alert=True)
    elif time_checking() == 'almost closed':
        await callback.answer('извините, мы можем не успеть сварить =(', show_alert=True)
    else:
        async with state.proxy() as data:
            data['address'] = callback_data['place']

        await callback.answer('принято')
        await callback.bot.send_chat_action(callback.message.chat.id, ChatActions.TYPING)
        await callback.message.answer('Веберите позиции из меню', reply_markup=keyboard_menu)


async def menu_call(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    db.insert('cart', {'chat_id': call.message.chat.id,
                       'item_name': callback_data['name'],
                       'quantity': 1})
    async with state.proxy() as data:
        data['cart_items'] = {}

    await call.answer('товар добавлен в корзину!')
    await call.message.answer('позиции находятся у вас в корзине', reply_markup=cart_markup)


async def show_cart(message: types.Message, state: FSMContext):
    cart_data = Cart().get_user_cart(message.chat.id)

    if len(cart_data) == 0:

        await message.answer('Ваша корзина пуста.')
        await message.delete()

    else:

        await message.bot.send_chat_action(message.chat.id, ChatActions.TYPING)

        order_cost = 0
        for i in cart_data:
            item = Menu().get_item(i['item_name'])
            order_cost += item['price']

            async with state.proxy() as data:
                data['cart_items'][item['name']] = [item['name'], item['size'], item['price'], i['quantity']]

                markup = item_markup(item['name'], i['quantity'])
                text = f'<b>{item["name"]}</b>\n\nЦена: {item["price"]}₽.'

                await message.answer(text, reply_markup=markup)

        if order_cost != 0:
            markup = ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add(order)

            await message.answer('Перейти к оформлению?',
                                 reply_markup=markup)


async def buttons(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    name = callback_data['name']
    action = callback_data['action']

    if action == 'count':

        async with state.proxy() as data:

            if 'cart_items' not in data.keys():

                await show_cart(call.message, state)

            else:

                await call.answer(f'{data["cart_items"][name][3]}шт')

    else:

        async with state.proxy() as data:

            if 'cart_items' not in data.keys():

                await show_cart(call.message, state)

            else:

                data['cart_items'][name][3] += 1 if action == 'increase' else -1
                count_in_cart = data['cart_items'][name][3]

                if count_in_cart == 0:

                    db.cond_delete('cart', 'chat_id', call.message.chat.id, 'item_name', name)
                    await call.message.delete()
                else:
                    db.cond_update('cart', 'quantity', count_in_cart, 'chat_id',
                                   call.message.chat.id, 'item_name', name)
                    await call.message.edit_reply_markup(item_markup(name, count_in_cart))


async def checkout_cart(message: types.Message, state: FSMContext):
    await FSMClient.check_cart.set()
    await checkout(message, state)


async def checkout(message, state: FSMContext):
    answer = ''
    total_price = 0

    async with state.proxy() as data:
        for item in data['cart_items'].values():
            total = item[3] * item[2]
            answer += f'<b>{item[0]}</b>, {item[3]}шт. = {total}₽\n'
            total_price += total
        data['cart_items']['total'] = total_price

    await message.answer(f'{answer}\nОбщая сумма заказа: {total_price}₽.'
                         f'\nСварим в кофейне по адресу:\n{data["address"]}',
                         reply_markup=check_markup())
    await FSMClient.confirm.set()


async def cart_back(message: types.Message, state: FSMContext):
    await state.finish()
    await send_welcome(message)


async def paying(call: types.Message, state: FSMContext):
    await FSMClient.confirm.set()
    token = call.bot['config'].tg_bot.youmoney
    async with state.proxy() as data:
        amount = data['cart_items']['total'] * 100

    await call.bot.send_invoice(chat_id=call.from_user.id, title='кофе', description=' ', payload='coffee',
                                provider_token=token, currency='RUB', start_parameter='test',
                                prices=[{'label': 'руб.', 'amount': amount}])


async def pre_checkout(check: types.PreCheckoutQuery):
    await check.bot.answer_pre_checkout_query(check.id, ok=True)


async def successful_payment(message: types.Message, state: FSMContext):
    if message.successful_payment.invoice_payload == 'coffee':
        customer = message.from_user.id
        answer = '<b>id покупателя: customer</b>\n'
        async with state.proxy() as data:
            adrs = data["address"]
            for item in data['cart_items'].values():
                answer += f'{item[0]}, размер: {item[1]}, цена: {item[3]}₽\n'
            admin = Address().get_admin(adrs)
        await message.bot.send_message(admin, text=answer)
        await message.bot.send_message(message.from_user.id, text='Мы уже начали варить ваш кофе!\n'
                                                                  'Как правило, ожидание составляет от пяти до '
                                                                  '20 минут.\nС вами свяжется бариста')


def register_handlers_client(dp: Dispatcher):
    dp.register_message_handler(send_welcome, commands=['start', ])
    dp.register_callback_query_handler(adrs_call, address_cd.filter())
    dp.register_callback_query_handler(menu_call, items_cd.filter(action='add'))
    dp.register_message_handler(show_cart, text=cart)
    dp.register_callback_query_handler(buttons, items_cd.filter(action='count'))
    dp.register_callback_query_handler(buttons, items_cd.filter(action='increase'))
    dp.register_callback_query_handler(buttons, items_cd.filter(action='decrease'))
    dp.register_message_handler(checkout_cart, text=order)
    dp.register_message_handler(cart_back, text=back_message, state='*')
    dp.register_message_handler(paying, text=ok_message, state=FSMClient.confirm)
    dp.register_pre_checkout_query_handler(pre_checkout)
    dp.register_message_handler(successful_payment, content_types=ContentType.SUCCESSFUL_PAYMENT)
