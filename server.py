import asyncio
import logging

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from config import load_config
from handlers import client, admin
from filters import OwnerFilter
# from middlewares.environment import EnvironmentMiddleware

logger = logging.getLogger(__name__)


# def register_middlewares(dp, config):
#     dp.setup_middleware(EnvironmentMiddleware(config=config))


def register_filters(dp):
    dp.filters_factory.bind(OwnerFilter)


def register_handlers(dp):
    admin.register_handlers_admin(dp)
    client.register_handlers_client(dp)


async def on_start():
    logging.basicConfig(
        level=logging.INFO,
        format=u'%(filename)s:%(lineno)d #%(levelname)-8s [%(asctime)s] - %(name)s - %(message)s',
    )

    config = load_config('.env')

    bot = Bot(token=config.tg_bot.token, parse_mode='HTML')
    storage = MemoryStorage()
    dp = Dispatcher(bot, storage=storage)
    bot['config'] = config
    # db.sql_create()
    # insert_db.sql_insert()

    #register_middlewares(dp)
    register_filters(dp)
    register_handlers(dp)

    print('бот запущен')

    try:
        await dp.start_polling()
    finally:
        await dp.storage.close()
        await dp.storage.wait_closed()
        await bot.session.close()


if __name__ == '__main__':
    try:
        asyncio.run(on_start())
    except (SystemExit, KeyboardInterrupt):
        logger.error('Бот остановлен')
