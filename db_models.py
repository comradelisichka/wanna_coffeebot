from abc import ABC, abstractmethod

from typing import List, Dict

import db


class DBModel(ABC):

    def __init__(self):
        self._attrs = self._load_attrs()

    @abstractmethod
    def _load_attrs(self) -> List[Dict]:
        pass


class Address(DBModel):

    def _load_attrs(self) -> List[Dict]:
        adrs = db.fetchall('addresses', ['name', ])
        return adrs

    def get_list_of_adrs(self) -> List:
        lst_adrs = []
        for a in self._attrs:
            for k, v in a.items():
                lst_adrs.append(v)

        return lst_adrs

    @staticmethod
    def get_admin(adrs):
        admins = db.fetchall('addresses', ['name', 'admin_id'])
        for a in admins:
            if a['name'] == adrs:
                return a['admin_id']

    @staticmethod
    def get_list_of_admins():
        admins = db.fetchall('addresses', ['name', 'admin_id'])
        lst_admins = []
        for a in admins:
            for k, v in a.items():
                if k == 'admin_id':
                    lst_admins.append(v)

        return lst_admins


class Menu(DBModel):

    def _load_attrs(self) -> List[Dict]:
        items = db.fetchall('menu', ['name', 'size', 'price'])
        return items

    def get_items(self):
        lst_items = []
        for a in self._attrs:
            lst_items.append(f"{a['name']}, {a['size']}мл, {a['price']}руб.")

        return lst_items

    def get_item(self, name) -> Dict:
        for i in self._attrs:
            if i['name'] == name:
                return i


class Cart(DBModel):

    def _load_attrs(self) -> List[Dict]:
        attrs = db.fetchall('cart', ['chat_id', 'item_name', 'quantity'])
        return attrs

    def get_user_cart(self, chat_id) -> List[Dict]:
        cart = []
        for a in self._attrs:
            if a['chat_id'] == chat_id:
                cart.append(a)
        return cart
