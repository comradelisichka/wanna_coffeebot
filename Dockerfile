FROM python:3.8 as base

COPY . .

WORKDIR .

RUN pip3 install pipenv

ENV PYTHONUNBUFFERED 1
ENV TZ=Europe/Moscow

ENTRYPOINT ["python", "server.py"]