import time


def time_checking():
    current_time = time.ctime().split(' ')
    prepared = current_time[3].split(':')
    if int(prepared[0]) < 9 or int(prepared[0]) > 21:
        return 'closed'
    elif int(prepared[0]) > 20 and int(prepared[1]) > 30:
        return 'almost closed'
    else:
        return 'open'

